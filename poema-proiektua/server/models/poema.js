//Require mongoose package
const mongoose = require('mongoose');

const poemakSchema = mongoose.Schema({    
    izena: String,
    autorea: String,
    urtea: Number,
    testua: String
},
{ collection: 'poemak' });
const poemak = module.exports = mongoose.model('poemak', poemakSchema );