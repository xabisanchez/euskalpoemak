const express = require('express');
const apiRouter = express.Router();

var poemaproiektua = require('../controllers/poema-proiektua')


apiRouter.route('/poemak')
    .get(poemaproiektua.findAllPoemak)
    .post(poemaproiektua.addPoema);
    
apiRouter.route('/poema/:id')
    .get(poemaproiektua.findPoemaById)
    .put(poemaproiektua.updatePoema)
    .delete(poemaproiektua.deletePoema);



/* GET api listing. */
apiRouter.get('/', (req, res) => {
  res.send('api works');
});


module.exports = apiRouter;