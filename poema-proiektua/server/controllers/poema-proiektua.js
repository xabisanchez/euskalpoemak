/* Zein Schema erabiliko dugun, kolekzio batekin lotuta*/
const poemak = require('../models/poema');



//GET - Return all Poemak in the DB
exports.findAllPoemak = function(req, res) {    
	poemak.find(function(err, poemak) {		
		if (err) return res.send(500, err.message);		
		console.log('GET /poemak');		
		res.status(200).jsonp(poemak);	
	});
};
//GET - Retun one poema in the DB
exports.findPoemaById = function(req, res) {
    poemak.findById(req.params.id, function(err, poema) {
    	if (err) return res.send(500, err.message);
		console.log('GET /poema/' + req.params.id);
		res.status(200).jsonp(poema);
    });
};

//POST - Insert a new poema in the DB
exports.addPoema = function(req, res) {
	console.log('POST');
	console.log(req.body);
	var poema = new poemak({
		izena: req.body.izena,
    	autorea: req.body.autorea,
		urtea: req.body.urtea,
    	testua: req.body.testua
	});
	poema.save(function(err, poema) {
		if (err) return res.status(500).send(err.message);
		console.log("Post-a gorde da!");
		res.status(200).jsonp(poema);
	});
};

//PUT - poema aldatu update
exports.updatePoema = function(req, res) {
	poemak.findById(req.params.id, function(err, poema) {
		poema.izena = req.body.izena;
		poema.autorea = req.body.autorea;
		poema.urtea = req.body.urtea;
    	poema.testua = req.body.testua;
    	poema.save(function(err){
    		if(err) return res.status(500).send(err.message);
    			res.status(200).json(poema);
    	});
	});
};

//DELETE - Delete one poema.
	exports.deletePoema = function(req, res) {	
	poemak.findById(req.params.id, function(err, poema) {
		poema.remove(function(err) {			
			if (err) return res.status(500).send(err.message);
			res.status(200).send();		
		})	
	});
};