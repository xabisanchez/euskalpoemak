import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PoemaService } from './../poema.service';
import { Poema } from './../poema';
import { Location } from '@angular/common';


@Component({
  selector: 'app-aldatu',
  templateUrl: './aldatu.component.html',
  styleUrls: ['./aldatu.component.css']
})
export class AldatuComponent implements OnInit {

  constructor(private location: Location, private route: ActivatedRoute, private poemaService: PoemaService) { }
  
  poema: Poema;
  getPoema(): void{
    var id = this.route.snapshot.paramMap.get('id');
    this.poemaService.PoemaBat(id)
      .subscribe(poema => {this.poema = poema},
    error => console.log("Error :: "+error));
  }
  
  PoemaAldatu(): void{
    this.poemaService.EditPoema(this.poema)
      .subscribe(res => {console.log(res); this.location.back();},
        err => {console.log("Errorea gertatu da");}
        );
  }
  
  ngOnInit() {
    this.getPoema();
  }

}
