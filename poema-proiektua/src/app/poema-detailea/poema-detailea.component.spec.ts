import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoemaDetaileaComponent } from './poema-detailea.component';

describe('PoemaDetaileaComponent', () => {
  let component: PoemaDetaileaComponent;
  let fixture: ComponentFixture<PoemaDetaileaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoemaDetaileaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoemaDetaileaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
