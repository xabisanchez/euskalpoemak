import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PoemaService } from './../poema.service';
import { Poema } from './../poema';

@Component({
  selector: 'app-poema-detailea',
  templateUrl: './poema-detailea.component.html',
  styleUrls: ['./poema-detailea.component.css']
})
export class PoemaDetaileaComponent implements OnInit {

  constructor(private route: ActivatedRoute, private poemaService: PoemaService) { }
  poema: Poema;
  getPoema(): void{
    var id = this.route.snapshot.paramMap.get('id');
    this.poemaService.PoemaBat(id)
      .subscribe(poema=>{this.poema=poema},
        error=>console.log("Error:: "+error));
  }

  ngOnInit() {
    this.getPoema();
  }

}
