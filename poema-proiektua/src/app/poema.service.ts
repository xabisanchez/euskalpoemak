import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Poema } from './poema';

@Injectable()
export class PoemaService {

  constructor(private http: HttpClient) { }
  
  //this.url parentesis edo giltzak?
  
  private Url ='/api';
  ListPoemak(): Observable<Poema[]>{
      let ListUrl = `${this.Url}/poemak/`;
      return this.http.get<Poema[]>(ListUrl)
  }
  
  PoemaBat(id: any): Observable<Poema>{
      let ListUrl = `${this.Url}/poema/`;
      return this.http.get<Poema>(ListUrl+id)
  }
  
   AddPoema(izena, autorea, urtea, testua){
    let ListUrl= `${this.Url}/poemak/`;
    const headers = new HttpHeaders();
    const body = {
        izena: izena,
        autorea: autorea,
        urtea: urtea,
        testua: testua
    };
    console.log(body);
    headers.append('Content-Type', 'application/json');
    return this.http.post(ListUrl, body, {headers: headers})
    }
    
    
    EditPoema(poema: Poema){
        let ListUrl = `${this.Url}/poema/${poema._id}`;
        
        const headers = new HttpHeaders();         
	    const body = {
	        izena: poema.izena, 
	        autorea: poema.autorea, 
	        urtea: poema.urtea, 
	        testua: poema.testua
	    };         
	    console.log(body);        	
	    headers.append('Content-Type', 'application/json');        
	    return this.http.put(ListUrl, body ,{headers: headers})    
    }
    
    DeletePoema(id){
        let ListUrl = `${this.Url}/poema/${id}`;
        return this.http.delete<Poema>(ListUrl);
     
    }

}
