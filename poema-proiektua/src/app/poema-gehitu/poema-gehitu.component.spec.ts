import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoemaGehituComponent } from './poema-gehitu.component';

describe('PoemaGehituComponent', () => {
  let component: PoemaGehituComponent;
  let fixture: ComponentFixture<PoemaGehituComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoemaGehituComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoemaGehituComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
