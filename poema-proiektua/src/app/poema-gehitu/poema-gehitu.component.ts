import { Component, OnInit } from '@angular/core';
import { PoemaService } from './../poema.service';
import { Poema } from './../poema';
import { Location } from '@angular/common';


@Component({
  selector: 'app-poema-gehitu',
  templateUrl: './poema-gehitu.component.html',
  styleUrls: ['./poema-gehitu.component.css']
})
export class PoemaGehituComponent implements OnInit {
public izena;
public autorea;
public urtea;
public testua;
constructor(private location: Location, private poemaService: PoemaService) { }

  PoemaBerria(): void{
    this.poemaService.AddPoema(this.izena, this.autorea, this.urtea, this.testua)
    .subscribe( res => {console.log(res); this.location.back();},
    err => {console.log("Errorea gertatu da");}
    );
  }
  
  

  ngOnInit() {
  }

}
