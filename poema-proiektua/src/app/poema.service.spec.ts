import { TestBed, inject } from '@angular/core/testing';

import { PoemaService } from './poema.service';

describe('PoemaService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PoemaService]
    });
  });

  it('should be created', inject([PoemaService], (service: PoemaService) => {
    expect(service).toBeTruthy();
  }));
});
