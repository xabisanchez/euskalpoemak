import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';


import { AppComponent } from './app.component';
import { BildumaComponent } from './bilduma/bilduma.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { HomeComponent } from './home/home.component';
import { PoemaService } from './poema.service';
import { PoemaDetaileaComponent } from './poema-detailea/poema-detailea.component';
import { PoemaGehituComponent } from './poema-gehitu/poema-gehitu.component';
import { AldatuComponent } from './aldatu/aldatu.component';



@NgModule({
  declarations: [
    AppComponent,
    BildumaComponent,
    HomeComponent,
    PoemaDetaileaComponent,
    PoemaGehituComponent,
    AldatuComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [PoemaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
