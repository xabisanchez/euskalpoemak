import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { BildumaComponent } from '../bilduma/bilduma.component';
import { HomeComponent } from '../home/home.component';
import { PoemaDetaileaComponent } from '../poema-detailea/poema-detailea.component';
import { PoemaGehituComponent } from '../poema-gehitu/poema-gehitu.component';
import { AldatuComponent } from '../aldatu/aldatu.component';

const routes: Routes=[
  {path: 'bilduma', component: BildumaComponent},
  {path: '', component: HomeComponent},
  {path: 'poema/:id', component: PoemaDetaileaComponent},
  {path: 'home', component: HomeComponent},
  {path: 'gehitu', component: PoemaGehituComponent},
  {path: 'aldatu/:id', component: AldatuComponent}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports:[
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
