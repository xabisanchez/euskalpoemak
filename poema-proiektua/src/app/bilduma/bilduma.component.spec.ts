import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BildumaComponent } from './bilduma.component';

describe('BildumaComponent', () => {
  let component: BildumaComponent;
  let fixture: ComponentFixture<BildumaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BildumaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BildumaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
