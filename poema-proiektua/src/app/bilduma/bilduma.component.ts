import { Component, OnInit } from '@angular/core';
import { PoemaService } from './../poema.service';
import { Poema } from './../poema';
import { Location } from '@angular/common';


@Component({
  selector: 'app-bilduma',
  templateUrl: './bilduma.component.html',
  styleUrls: ['./bilduma.component.css']
})
export class BildumaComponent implements OnInit {

  constructor(private location: Location, private poema: PoemaService) { }
  
  poemak: Poema[];
  
  getPoemak(): void{
    this.poema.ListPoemak()
      .subscribe(poemak => this.poemak = poemak);
  }
  
  PoemaEzabatu(id: any): void {    
	this.poema.DeletePoema(id)    
		.subscribe( res => {console.log(res); this.location.back();},        
				     err => {console.log("Errorea gertatu da");}    
				);    
  }
  
  ngOnInit() {
    this.getPoemak();
  }

}
