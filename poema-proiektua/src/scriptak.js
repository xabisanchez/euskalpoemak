  $(document).ready(function(){
    
    
    $("#day").click(function(){
    $("#detaile-osoa").css('backgroundColor','white');
    $("#detaile-osoa").css('color','black');
    });
   
    $("#night").click(function(){
       $("#detaile-osoa").css('backgroundColor','#3c3c3d');
       $("#detaile-osoa").css('color','white');
    });
    //fadein-ak
    
    $("#ataria").hide();
    $("#ataria").fadeIn("slow");
    $("#detaile-osoa").hide();
    $("#detaile-osoa").fadeIn("slow");
    $("#bilduma-osoa").hide();
    $("#bilduma-osoa").fadeIn("slow");
    
    $("#nav").click(function(){
        $("#ataria").hide();
        $("#ataria").fadeIn("slow");
        $("#bilduma-osoa").hide();
        $("#bilduma-osoa").fadeIn("slow");
    });
    
    $("#sartualbumera").click(function(){
        $("#bilduma-osoa").hide();
        $("#bilduma-osoa").fadeIn("slow");
    });
    
    $("#bildumakoLink").click(function(){
        $("#detaile-osoa").hide();
        $("#detaile-osoa").fadeIn("slow");
    });
    
    
    $("#gehitzekoForm").submit(function(){
        this.reset();
    });
    
    $("#aldatuBotoia").click(function() {
        $("#aldatuOharra").text("aldaketa egin duzu!");
    });
    
    $("#ezabatzekoBotoia").click(function(){
        location.reload();
    });
    

  });